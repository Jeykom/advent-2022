import fs from 'fs'

const treeGrid = []

function eight() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const treeRows = data.split(/\n/)

    let visibleTrees = 0
    treeRows.forEach((row, rowIndex) => {
      if (row === '') return

      const trees = row.split('')

      treeGrid.push([])

      trees.forEach((tree, columnIndex) => {

        treeGrid[rowIndex].push(tree)
      })
    })

    treeGrid.forEach((row, rowIndex) => {

      row.forEach((tree, columnIndex) => {
        // checkLeft
        let visibleLeft = true
        for (let i = columnIndex - 1; i >= 0 ; i--) {
          if (row[i] >= tree) {
            visibleLeft = false
            break
          }
        }
        if (visibleLeft) {
          visibleTrees += 1
          return
        }

        // check right
        let visibleRight = true
        for (let i = columnIndex + 1; i < row.length; i++) {
          if (row[i] >= tree) {
            visibleRight = false
            break
          }
        }
        if (visibleRight) {
          visibleTrees += 1
          return
        }

        // check bottom
        let visibleBottom = true
        for (let i = rowIndex + 1; i < treeGrid.length; i++) {
          if (treeGrid[i][columnIndex] >= tree) {
            visibleBottom = false
            break
          }
        }
        if (visibleBottom) {
          visibleTrees += 1
          return
        }

        // checkTop
        let visibleTop = true
        for (let i = rowIndex - 1; i >= 0 ; i--) {
          if (treeGrid[i][columnIndex] >= tree) {
            visibleTop = false
            break
          }
        }
        if ( visibleTop) {
          visibleTrees += 1
          return
        }
      })
    })
    console.log('visibleTrees', visibleTrees)
  })
}

eight()
