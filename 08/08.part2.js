import fs from 'fs'

const treeGrid = []

function eight() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const treeRows = data.split(/\n/)

    const scenicScores = []
    treeRows.forEach((row, rowIndex) => {
      if (row === '') return

      const trees = row.split('')

      treeGrid.push([])

      trees.forEach((tree, columnIndex) => {

        treeGrid[rowIndex].push(tree)
      })
    })

    treeGrid.forEach((row, rowIndex) => {
      row.forEach((tree, columnIndex) => {
        // checkLeft
        let treesSeenLeft = 0
        for (let i = columnIndex - 1; i >= 0 ; i--) {
          treesSeenLeft += 1
          if (row[i] >= tree) break
        }

        // check right
        let treesSeenRight = 0
        for (let i = columnIndex + 1; i < row.length; i++) {
          treesSeenRight += 1
          if (row[i] >= tree) break
        }

        // check bottom
        let treesSeenBottom = 0
        for (let i = rowIndex + 1; i < treeGrid.length; i++) {
          treesSeenBottom += 1
          if (treeGrid[i][columnIndex] >= tree) break
        }

        // checkTop
        let treesSeenTop = 0
        for (let i = rowIndex - 1; i >= 0 ; i--) {
          treesSeenTop += 1
          if (treeGrid[i][columnIndex] >= tree) break
        }

        const scenicScore = treesSeenLeft * treesSeenRight * treesSeenBottom * treesSeenTop

        scenicScores.push(scenicScore)
      })
    })
    
    scenicScores.sort((a, b) => b - a)
    console.log('highest score', scenicScores[0])
  })
}

eight()
