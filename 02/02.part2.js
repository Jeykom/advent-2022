import fs from 'fs'

const REQUESTS = {
  Rock: 'A',
  Paper: 'B',
  Scissors: 'C'
}

const RESULTS = {
  lose: 'X',
  draw: 'Y',
  win: 'Z'
}

const POINTS = {
  Rock: 1,
  Paper: 2,
  Scissors: 3,
  lose: 0,
  draw: 3,
  win: 6
}

function calcRoundPoints(request, neededResult) {
  if (request === REQUESTS.Rock) {
    if (neededResult === RESULTS.lose) {
      return POINTS.lose + POINTS.Scissors
    }
    if (neededResult === RESULTS.draw) {
      return POINTS.draw + POINTS.Rock
    }
    if (neededResult === RESULTS.win) {
      return POINTS.win + POINTS.Paper
    }
  }
  if (request === REQUESTS.Paper) {
    if (neededResult === RESULTS.lose) {
      return POINTS.lose + POINTS.Rock
    }
    if (neededResult === RESULTS.draw) {
      return POINTS.draw + POINTS.Paper
    }
    if (neededResult === RESULTS.win) {
      return POINTS.win + POINTS.Scissors
    }
  }
  if (request === REQUESTS.Scissors) {
    if (neededResult === RESULTS.lose) {
      return POINTS.lose + POINTS.Paper
    }
    if (neededResult === RESULTS.draw) {
      return POINTS.draw + POINTS.Scissors
    }
    if (neededResult === RESULTS.win) {
      return POINTS.win + POINTS.Rock
    }
  }
}

function two() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err) {
      console.log('error', err)
      return
    }

    const rounds = data.split(/\n/)
    let totalPoints = 0

    rounds.forEach((round) => {
      if (round === '') return

      const roundArray = round.split(' ')
      const request = roundArray[0]
      const neededResult = roundArray[1]

      const winningPoints = calcRoundPoints(request, neededResult)

      totalPoints += winningPoints
      console.log('round', round)
      console.log('points', winningPoints)
    })

    console.log('totalPoints', totalPoints)
  })
}

two()
