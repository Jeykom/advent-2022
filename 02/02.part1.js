import fs from 'fs'

const REQUESTS = {
  Rock: 'A',
  Paper: 'B',
  Scissors: 'C'
}

const RESPONSES = {
  Rock: 'X',
  Paper: 'Y',
  Scissors: 'Z'
}

const POINTS = {
  X: 1,
  Y: 2,
  Z: 3,
  lose: 0,
  draw: 3,
  win: 6
}

function calcWinningPoints(request, response) {
  if (request === REQUESTS.Rock) {
    if (response === RESPONSES.Scissors) return POINTS.lose
    if (response === RESPONSES.Rock) return POINTS.draw
    if (response === RESPONSES.Paper) return POINTS.win
  }

  if (request === REQUESTS.Paper) {
    if (response === RESPONSES.Scissors) return POINTS.win
    if (response === RESPONSES.Rock) return POINTS.lose
    if (response === RESPONSES.Paper) return POINTS.draw
  }

  if (request === REQUESTS.Scissors) {
    if (response === RESPONSES.Scissors) return POINTS.draw
    if (response === RESPONSES.Rock) return POINTS.win
    if (response === RESPONSES.Paper) return POINTS.lose
  }
}

function two() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err) {
      console.log('error', err)
      return
    }

    const rounds = data.split(/\n/)
    let totalPoints = 0

    rounds.forEach((round) => {
      if (round === '') return

      const roundArray = round.split(' ')
      const request = roundArray[0]
      const response = roundArray[1]

      const shapePoints = POINTS[response]
      const winningPoints = calcWinningPoints(request, response)

      totalPoints += shapePoints + winningPoints
    })

    console.log('totalPoints', totalPoints)
  })
}

two()
