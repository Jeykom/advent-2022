import fs from 'fs'


function generateStacks(stacksString) {
  const stackRows = stacksString.split(/\n/)
  const stackCountRow = stackRows.splice( -1)[0]

  const stacks = {}

  stackRows.reverse()
  stackCountRow.split('').forEach((count, index) => {
    if (count === ' ') return

    if (!stacks[count]) stacks[count] = []

    stackRows.forEach((row) => {
      const supply = row[index]

      if (supply === ' ' || !supply) return


      stacks[count].push(supply)
    })
  })

  return stacks
}

function moveStack(fromStack, toStack, moveCount) {
  for (let i = 0; i < moveCount; i++) {
    const supply = fromStack.pop()
    toStack.push(supply)
  }
}

function five() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const [stacksString, instructionsString] = data.split(/\ns*\n/)

    const stacks = generateStacks(stacksString)

    instructionsString.split(/\n/).forEach((instructionString) => {
      if (instructionString === '') return

      const splitInstructionsString = instructionString.split(' ')
      const moveCount = parseInt(splitInstructionsString[1])
      const fromStackIndex = splitInstructionsString[3]
      const toStackIndex = splitInstructionsString[5]

      moveStack(stacks[fromStackIndex], stacks[toStackIndex], moveCount)
    })

    const result = Object.values(stacks).reduce((finalString, currStack) => {
      const supply = currStack.pop()
      return finalString + supply
    }, '')

    console.log('result', result)
  })
}

five()
