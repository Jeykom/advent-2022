import fs from 'fs'

function one() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const elvesArray = data.split(/\ns*\n/)

    const allElvesTotalCals = elvesArray.map((calString) => {
      const calsAsArray = calString.split(/\n/)
      const totalCals = calsAsArray.reduce((lastTotal, currentCal) => {
        if (currentCal === '') return lastTotal

        return lastTotal + parseInt(currentCal)
      }, 0)

      return totalCals
    })

    allElvesTotalCals.sort((a, b) => a-b)
    allElvesTotalCals.reverse()
    console.log('hightest Cal', allElvesTotalCals[0])
    const highestThreeTotals = allElvesTotalCals[0] + allElvesTotalCals[1] + allElvesTotalCals[2]
    console.log('hightest three', highestThreeTotals)

  })
}

one()
