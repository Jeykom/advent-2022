import fs from 'fs'

const PRIOS = {}

for (let i = 1; i <= 26; i++) {
  const letter = String.fromCharCode(96 + i)

  PRIOS[letter] = i
}
for (let i = 27; i <= 52; i++) {

  const letter = String.fromCharCode(64 -26 + i)

  PRIOS[letter] = i
}

function three() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const rucksacksArray = data.split(/\n/)

    let prioSum = 0

    rucksacksArray.forEach((rucksack) => {
      if (rucksack === '') return
      const rucksackArray = rucksack.split('')

      const halfItemCount = Math.ceil(rucksackArray.length / 2);

      const firstCompartment = rucksackArray.slice(0, halfItemCount)
      const secondCompartment = rucksackArray.slice(halfItemCount, rucksackArray.length)

      const sharedItems = firstCompartment.filter((letter) => {
        return secondCompartment.includes(letter)
      })
      new Set(sharedItems).forEach((sharedItem) => {
        prioSum += PRIOS[sharedItem]
      })

    })
    console.log('sum', prioSum)
  })
}

three()
