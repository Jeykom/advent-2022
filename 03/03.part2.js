import fs from 'fs'

const PRIOS = {}

for (let i = 1; i <= 26; i++) {
  const letter = String.fromCharCode(96 + i)

  PRIOS[letter] = i
}
for (let i = 27; i <= 52; i++) {

  const letter = String.fromCharCode(64 -26 + i)

  PRIOS[letter] = i
}

function three() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const rucksacksArray = data.split(/\n/)
    const ruckSackGroups = []

    while (rucksacksArray.length > 0) {
      ruckSackGroups.push(rucksacksArray.splice(0, 3))
    }

    console.log('groups', ruckSackGroups)

    let prioSum = 0

    ruckSackGroups.forEach((rucksackGroup) => {
      if (rucksackGroup.length < 3) return

      const group1 = rucksackGroup[0].split('')
      const group2 = rucksackGroup[1].split('')
      const group3 = rucksackGroup[2].split('')

      const sharedItems = group1.filter((letter) => {
        return group2.includes(letter) && group3.includes(letter)
      })

      new Set(sharedItems).forEach((sharedItem) => {
        prioSum += PRIOS[sharedItem]
      })

    })
    console.log('sum', prioSum)
  })
}

three()
