import fs from 'fs'

const currentDirs = []
const dirSizes = {}

function addToCurrentDirList(dir) {
  currentDirs.push(dir)
}

function removeLastDirFromList() {
  currentDirs.pop()
}

function addFileSizeToDirs(fileSize) {
  currentDirs.forEach((dir, index) => {
    let uniqueDirName = ''
    for (let i = 0; i <= index; i++) {
      uniqueDirName += currentDirs[i]
    }

    if (!dirSizes[uniqueDirName]) dirSizes[uniqueDirName] = 0

    dirSizes[uniqueDirName] += fileSize
  })
}

function seven() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const commandList = data.split(/\n/)
    let prefix = ''

    commandList.forEach((command, i) => {
      if (command === '') return

      const splitCommand = command.split(' ')

      if (splitCommand[0] === '$') {
        if (splitCommand[1] === 'ls') {
          return
        } else if (splitCommand[1] === 'cd') {
          if (splitCommand[2] === '..') {
            prefix =prefix.substr(0, -1)
            removeLastDirFromList()
          } else {
            addToCurrentDirList(splitCommand[2])
            prefix += '-'
          }
        }
      } else if (splitCommand[0] === 'dir') {
        return
      } else {
        addFileSizeToDirs(parseInt(splitCommand[0]))
      }

    })

    let totalSize = 0
    Object.values(dirSizes).forEach((size) => {
      if (size <= 100000) {
        totalSize += size
      }
    })

    console.log('totalSize', totalSize)
  })
}

seven()
