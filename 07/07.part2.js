import fs from 'fs'

const TOTAL_SPACE = 70000000
const SPACE_NEEDED_FOR_UPDATE = 30000000

const currentDirs = []
const dirSizes = {}

function addToCurrentDirList(dir) {
  currentDirs.push(dir)
}

function removeLastDirFromList() {
  currentDirs.pop()
}

function addFileSizeToDirs(fileSize) {
  currentDirs.forEach((dir, index) => {
    let uniqueDirName = ''
    for (let i = 0; i <= index; i++) {
      uniqueDirName += currentDirs[i]
    }

    if (!dirSizes[uniqueDirName]) dirSizes[uniqueDirName] = 0

    dirSizes[uniqueDirName] += fileSize
  })
}

function seven() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const commandList = data.split(/\n/)
    let prefix = ''

    commandList.forEach((command, i) => {
      if (command === '') return

      const splitCommand = command.split(' ')

      if (splitCommand[0] === '$') {
        if (splitCommand[1] === 'ls') {
          return
        } else if (splitCommand[1] === 'cd') {
          if (splitCommand[2] === '..') {
            prefix =prefix.substr(0, -1)
            removeLastDirFromList()
          } else {
            addToCurrentDirList(splitCommand[2])
            prefix += '-'
          }
        }
      } else if (splitCommand[0] === 'dir') {
        return
      } else {
        addFileSizeToDirs(parseInt(splitCommand[0]))
      }

    })

    const possibleDirsToDelete = []
    const unusedSpace = TOTAL_SPACE - dirSizes['/']
    const spaceNeededToDelete = SPACE_NEEDED_FOR_UPDATE - unusedSpace

    Object.values(dirSizes).forEach((size) => {
      if (size >= spaceNeededToDelete) {
        possibleDirsToDelete.push(size)
      }
    })
    
    possibleDirsToDelete.sort((a, b) => a - b)

    console.log('smalles to delete', possibleDirsToDelete[0])
  })
}

seven()
