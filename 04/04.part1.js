import fs from 'fs'

function convertRangeToArray(rangeString) {
  const split = rangeString.split('-')
  const from = parseInt(split[0])
  const to = parseInt(split[1])

  const rangeArray = []

  for (let i = from; i <= to; i++) {
    rangeArray.push(i)
  }

  return rangeArray
}

function isArrayContainingArray(array1, array2) {
  return array1.every((val) => {
    return array2.includes(val)
  })
}

function four() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    const elvesPairs = data.split(/\n/)

    let overlaps = 0

    elvesPairs.forEach((pair) => {
      if (pair === '') return

      const splitPair = pair.split(',')
      const elfOne = splitPair[0]
      const elfTwo = splitPair[1]

      const rangeOne = convertRangeToArray(elfOne)
      const rangeTwo = convertRangeToArray(elfTwo)

      if (isArrayContainingArray(rangeOne, rangeTwo) || isArrayContainingArray(rangeTwo, rangeOne)) {
        overlaps += 1
      }
    })

    console.log('overlaps', overlaps)
  })
}

four()
