import fs from 'fs'

function six() {
  fs.readFile('./input.txt', 'utf8', (err, data) => {
    if (err){
      console.log('error', err)
      return
    }

    let charArray = []
    let signalStart

    data.split('').some((char, index) => {
      const indexOfChar = charArray.indexOf(char)
      if (indexOfChar >= 0) {
        charArray.splice(0, indexOfChar + 1)
      }

      charArray.push(char)

      if (charArray.length === 4) {
        signalStart = index + 1
        return true
      }

      return false
    })

    console.log('start', signalStart)
  })
}

six()
